package com.acalcina;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends AppCompatActivity {

    EditText usu,pass;
    Button btnlogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        usu = (EditText) findViewById(R.id.txtusu);
        pass = (EditText) findViewById(R.id.txtpas);
        btnlogin = (Button) findViewById(R.id.btningresar);

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usuario = usu.getText().toString();
                String paswd = pass.getText().toString();

                Intent i = new Intent(getApplicationContext(),Principal.class);

                if(usuario.equals("ander") && (paswd.equals("123"))){
                    startActivity(i);
                }else{
                    Toast.makeText(Login.this, "Usuario o Contraseña Incorrecta", Toast.LENGTH_SHORT).show();
                }


            }
        });

    }
}