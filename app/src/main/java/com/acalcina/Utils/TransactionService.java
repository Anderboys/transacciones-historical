package com.acalcina.Utils;

import com.acalcina.Model.Transaction;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

// paso 2. creamos sus metodos
public interface TransactionService {

    @GET("listar")
    Call<List<Transaction>> getPersonas();

    @POST("agregar")
    Call<Transaction>addPersona(@Body Transaction persona);

    @POST("actualizar/{id}")
    Call<Transaction>updatePersona(@Body Transaction persona,@Path("id") int id);

    @POST("eliminar/{id}")
    Call<Transaction>deletePersona(@Path("id")int id);

}
