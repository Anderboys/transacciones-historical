package com.acalcina.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.acalcina.Model.Transaction;
import com.acalcina.R;

import java.util.List;

// paso 5. despues de crear el item_transaction.xml
//5.1 seguimos aqui
public class TransactionAdapter extends ArrayAdapter<Transaction> {

    private Context context;
    private  List<Transaction>transactions;

    //5.2 crear constructor TransactionAdapter q recibe 3 parametros
    public TransactionAdapter(@NonNull Context context, int resource, @NonNull List<Transaction> objects) {
        super(context, resource, objects);
        this.context=context;
        this.transactions=objects;
    }

    //5.3 public View getView
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView=layoutInflater.inflate(R.layout.item_transaction,parent,false);
        TextView IdTrans=(TextView)rowView.findViewById(R.id.Id_Transation);
        TextView Amount=(TextView)rowView.findViewById(R.id.Amount);
        TextView Type=(TextView)rowView.findViewById(R.id.Type);
        TextView AccountId=(TextView)rowView.findViewById(R.id.AccountId);

        IdTrans.setText(String.format("%s",transactions.get(position).getId()));
        Amount.setText(String.format("%s",transactions.get(position).getAmount()));
        Type.setText(String.format("%s",transactions.get(position).getType()));
        AccountId.setText(String.format("%s",transactions.get(position).getAccountId()));
//        IdTrans.setText(String.format("ID TRANSACTION: %s",transactions.get(position).getId()));


        // recibiendo datos por click  putExtra
//        rowView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent=new Intent(context, TransactionActivity.class);
//                intent.putExtra("ID",String.valueOf(transactions.get(position).getId()));
//                intent.putExtra("AMOUNT",transactions.get(position).getAmount());
//                intent.putExtra("TYPE",transactions.get(position).getType());
//                intent.putExtra("ACCOUNT_ID",transactions.get(position).getAccountId());
//                context.startActivity(intent);
//            }
//        });
        return rowView;

    }


}
