package com.acalcina;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.acalcina.Adapter.TransactionAdapter;
import com.acalcina.Model.Transaction;
import com.acalcina.Utils.Apis;
import com.acalcina.Utils.TransactionService;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.acalcina.databinding.PrincipalBinding;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Principal extends AppCompatActivity {

    //6. instanciamos

    private PrincipalBinding binding;
    TransactionService transactionService;
    List<Transaction> listPersona = new ArrayList<>();
    ListView listView;
    FloatingActionButton fab;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //7. asignamos lo valores
        // detalle: PrincipalBinding  para q funcione agregar en el buil.gradle
//        buildFeatures {
//            viewBinding true
//        }
       binding = PrincipalBinding.inflate(getLayoutInflater());
       setContentView(binding.getRoot());
       listView = (ListView) findViewById(R.id.listView);

      //  listTransaction();

        fab = findViewById(R.id.fabe);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                listTransaction();


            }
        });



    }

    // paso 8. creando metodo listar persona
    public void listTransaction() {

        transactionService = Apis.getTransactionService();
        Call<List<Transaction>> call = transactionService.getPersonas();
        call.enqueue(new Callback<List<Transaction>>() {
            @Override
            public void onResponse(Call<List<Transaction>> call, Response<List<Transaction>> response) {
                if (response.isSuccessful()) {
                    // cargar datos en 1 List<Transaction> listPersona = new ArrayList<>();
                    listPersona = response.body();
                    // Asignar todos los Datos en 1 ListView con el metodo setAdapter
                    listView.setAdapter(new TransactionAdapter(Principal.this, R.layout.item_transaction, listPersona));
                }
            }
            @Override
            public void onFailure(Call<List<Transaction>> call, Throwable t) {
                Log.e("Error:", t.getMessage());
            }
        });
    }


    // 1.  ------------ this is For ACTION BAR -------------
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_salir, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.logout:
                Intent intent = new Intent(getApplicationContext(),Login.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    //----------------------------------------------------------------

}